module Spree
  class Engine < Rails::Engine
      # Add a load path for this specific Engine
      config.to_prepare do
        Dir.glob(File.join(File.dirname(__FILE__), "../../app/overrides/**.*")) do |c|
          require_dependency c
        end
      end
  end
end