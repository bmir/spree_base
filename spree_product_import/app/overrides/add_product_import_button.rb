require 'deface'

class AddProductImportButton
  Deface::Override.new(virtual_path: 'admin/products/index',
    name: 'add_product_import_button',
    #insert_after: "erb[loud]:contains('button_link_to Spree.t(:new_product)')",
    insert_after: '<div class="page-actions pl-0 pr-0 d-none d-lg-flex" data-hook="toolbar">',
    text: '<h1>A new part added!</h1>')
end